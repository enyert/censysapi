﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Linq;
using Unit_Tests.DAL.EntityModels;
using Unit_Tests.DAL.Infrastructure;

namespace Unit_Tests
{
    [TestClass]
    public class UnitTest1
    {
        Idioma _idioma1;
        Idioma _idioma2;
        Idioma _idioma3;
        MockCensysDataContext _context;
        MockRepository<Idioma> _repo;

        [TestInitialize]
        public void Initialize()
        {
            _context = new MockCensysDataContext();
            _idioma1 = new Idioma();
            _repo = new MockRepository<Idioma>(_context);
            _idioma1.Nombre = "Holaaaa";
            _idioma1.PkIdioma = 3;
            _idioma1.Descripcion = "VASDASDAS";

            _idioma2 = new Idioma();
            _idioma2.Nombre = "Hola";
            _idioma2.PkIdioma = 4;
            _idioma2.Descripcion = "sadasdas";

            _idioma3 = new Idioma();
            _idioma3.Nombre = "Hola";
            _idioma3.PkIdioma = 5;
            _idioma3.Descripcion = "sadasdas";

        }
        
        //[TestMethod]
        //public void TestAddRepository()
        //{
        //    Assert.IsTrue(_repo.Add(_idioma3));
        //}

        //[TestMethod]
        //public void TestDeleteRepository()
        //{
        //    Assert.IsTrue(_repo.Delete(_idioma1));
        //}

        [TestMethod]
        public void TestUpdateRepository()
        {
            Assert.IsTrue(_repo.Update(_idioma1));
        }

    }
}
