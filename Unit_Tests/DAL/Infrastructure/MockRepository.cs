﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit_Tests.DAL.EntityModels;
using Unit_Tests.DAL.Interfaces;

namespace Unit_Tests.DAL.Infrastructure
{
    class MockRepository<TEntity> : MockIRepository<TEntity>
    where TEntity : class
        {
            MockCensysDataContext _db;
            public MockRepository()
            {
                _db = new MockCensysDataContext();
            }

            public MockRepository(MockCensysDataContext _context)
            {
                _db = _context;
            }

            public Boolean Add(TEntity _entity)
            {
                try
                {
                    if (_entity == null)
                    {
                        throw new ArgumentNullException("Problema con la entidad");
                    }
                    var _add = _db.GetTable<TEntity>();
                    _add.InsertOnSubmit(_entity);
                    _db.SubmitChanges();
                    return true;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            public Boolean Delete(TEntity _entity)
            {
                try
                {
                    var _del = _db.GetTable<TEntity>();
                    _del.Attach(_entity);
                    _del.DeleteOnSubmit(_entity);
                    _db.SubmitChanges();
                    return true;
                }
                catch (Exception)
                {
                    throw;
                }
            }

            public IList<TEntity> GetAll()
            {
                try
                {
                    var _list = _db.GetTable<TEntity>().ToList<TEntity>();
                    return _list;
                }
                catch (Exception)
                {
                    throw;
                }
            }

            //public long Count()
            //{
            //    return this..LongCount<TEntity>();
            //}

            public Boolean Update(TEntity _entity)
            {
                try
                {
                    var _update = _db.GetTable<TEntity>();
                    _update.Attach(_entity);
                    _db.Refresh(RefreshMode.KeepCurrentValues, _entity); //This is a hack but it works
                    _db.SubmitChanges();

                    return true;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
}
