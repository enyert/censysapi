﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit_Tests.DAL.Interfaces
{
       
    public interface MockIRepository<T> where T : class
        {
            Boolean Add(T _entity);

            Boolean Delete(T _entity);

            IList<T> GetAll();

            //long Count();

            Boolean Update(T _entity);

            //T GetById(object _id);
        }
}
