﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Text;
using System.ComponentModel;  
using System.Data.Entity;
using CensysAPI.DAL.EntityModels;

namespace CensysAPI.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {

        Boolean Add(T _entity);
              
        Boolean Delete(T _entity);

        IList<T> GetAll(); 

        //long Count();

        Boolean Update(T _entity);

        //T GetById(object _id);
    }
}