﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CensysAPI.DAL.EntityModels;
using CensysAPI.DAL.Interfaces;

namespace CensysAPI.DAL.Infrastructure
{
    interface AlertDAL : Repository<Alert> , IAlertDAL
    {
    }
}
