﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CensysAPI.Models;

namespace CensysAPI.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            CrearTipoEncuesta();
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {   
            return "value";
        }

        public void CrearTipoEncuesta()
        {

            CensysDataContext _db2 = new CensysDataContext();
            Tipo_Encuesta _tencuesta = new Tipo_Encuesta();
            Seccion _seccion = new Seccion();
            Pregunta _pregunta = new Pregunta();

            _tencuesta.PkEncuesta = 3;
            _seccion.PkSeccion = 3;
            _pregunta.PkPregunta = 3;
            _pregunta.FkSeccion = 3;
            _seccion.FkEncuesta = 3;



            _pregunta.Titulo = "Hola mundo";
            _pregunta.FechaCreacion = DateTime.UtcNow.Date;
            _seccion.Nombre = "Cesar";
            _tencuesta.Tipo = "Elegante";
            _tencuesta.Status = 1;


            _db2.Tipo_Encuestas.InsertOnSubmit(_tencuesta);
            _db2.Seccions.InsertOnSubmit(_seccion);
            _db2.Preguntas.InsertOnSubmit(_pregunta);
            _db2.SubmitChanges();

        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}