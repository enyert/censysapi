﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CensysAPI.Models;

namespace CensysAPI.Controllers
{
    public class ManiController : Controller
    {
        //
        // GET: /Mani/

        public ActionResult Index()
        {
            CrearTipoEncuesta();
            return View();
        }


        public void CrearTipoEncuesta()
        {

            CensysDataContext _db2 = new CensysDataContext();
            Tipo_Encuesta _tencuesta = new Tipo_Encuesta();
            Seccion _seccion = new Seccion();
            Pregunta _pregunta = new Pregunta();

                _tencuesta.PkEncuesta = 3;
                _seccion.PkSeccion = 3;
                _pregunta.PkPregunta = 3;
                _pregunta.FkSeccion = 3;
                _seccion.FkEncuesta = 3;

            

            _pregunta.Titulo = "Hola mundo" ;
            _pregunta.FechaCreacion = DateTime.UtcNow.Date;
            _seccion.Nombre = "Cesar";
            _tencuesta.Tipo = "Elegante";
            _tencuesta.Status = 1;


            _db2.Tipo_Encuestas.InsertOnSubmit(_tencuesta);
            _db2.Seccions.InsertOnSubmit(_seccion);
            _db2.Preguntas.InsertOnSubmit(_pregunta);
            _db2.SubmitChanges();

        }
    }
}
