using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using CensysAPI.DAL.EntityModels;
using CensysAPI.DAL.Infrastructure;
using CensysAPI.BLL;
using NUnit.Framework;


namespace CensysAPI
{
    [TestFixture]
    public class Tests
    {
        //private Mock<CensysDataContext> _mockDataContext;
        Idioma _idioma;
        Probando _probando;
        CensysDataContext _context;
        Repository<Idioma> _repo;

        [SetUp]
        public void Initialize()
        {
            _context = new CensysDataContext();
            _probando = new Probando();
            _idioma = new Idioma();
        }

        [Test]
        public void TestAdd()
        {
            _repo = new Repository<Idioma>();
            _idioma.Nombre = "Hola";
            _idioma.PkIdioma = 3;
            _idioma.Descripcion = "sadasdas";

            Assert.IsTrue(_repo.Add(_idioma));
        }
    }
}
